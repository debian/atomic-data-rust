#!/usr/bin/make -f

# resolve DEB_VERSION_UPSTREAM DEB_DISTRIBUTION
include /usr/share/dpkg/pkg-info.mk

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic
export DEB_CXXFLAGS_MAINT_APPEND  = -Wall -pedantic

# trust upstream optimization level unless explicitly disabled
ifeq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
export DEB_CFLAGS_MAINT_STRIP=-O2
export DEB_CXXFLAGS_MAINT_STRIP=-O2
endif

# resolve if release is experimental
DEB_SUITE_EXP = $(filter experimental% UNRELEASED,$(DEB_DISTRIBUTION))

# generate documentation unless nodoc requested
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
MANPAGES = debian/atomic-cli.1 debian/atomic-server.8
endif

# generate manual pages from output of clap-based Rust binary
#  arguments: 1:MANFILEPATH 2:COMMANDPATH 3:SUBCOMMAND(S)
_help2man = \
 $(eval suffix = $(lastword $(subst ., ,$(1))))\
 $(eval stem = $(patsubst %.$(suffix),%,$(1)))\
 help2man --section $(suffix) --no-info \
 --version-string='$(DEB_VERSION_UPSTREAM)' \
 --name $(shell $(2) $(3) --help | head --lines=1 | xargs -0 shell-quote --) \
 --output '$(stem)$(if $(3),-$(subst $() ,-,$(strip $(3)))).$(suffix)' \
 '$(2)$(if $(3), $(3))' \
 || { $(2) $(3) --help; false; }

# resolve all direct entries in first "package..." section in TODO
VENDORLIBS = $(shell perl -nE \
 '/^(?:\s{2}[*]\s*(?:(package)|(\S+))|\s{4}[*]\s*(\S+))/ or next; \
 $$i++ if $$1; exit if $$i > 1 or $$2; say $$3 if $$3 and $$i;' \
 debian/TODO)

TEST_NEED_NET += \
 test::get_path \
 test::get_path_array \
 test::get_url \
 test::set_and_get \
 client::test::fetch_resource_basic \
 collections::test::get_collection_params \
 parse::test::serialize_parse_roundtrip

comma = ,
_mangle = perl -gpi \
 -e 's/$(1)\W+version = "\K\Q$(subst |,$(comma),$(2))"/$(subst |,$(comma),$(3))"/g;' \
 $(patsubst %,debian/vendorlibs/%/Cargo.toml,$(4))

%:
	dh $@ --buildsystem rust

execute_before_dh_auto_configure:
# FIXME: package and symlink actual web content instead
	mkdir --parents server/assets_tmp
	touch server/assets_tmp/index.html
	cp --force --recursive --target-directory=server/assets_tmp browser/data-browser/src/*
	touch server/assets_tmp/index.html

override_dh_auto_test:
	$(if $(DEB_SUITE_EXP),\
	 dh_auto_test --buildsystem rust -- --no-fail-fast -- --include-ignored || true,\
	 dh_auto_test --buildsystem rust -- --no-fail-fast -- $(addprefix --skip ,$(TEST_NEED_NET)))

execute_after_dh_auto_install-arch: $(MANPAGES)
	mkdir --parents debian/atomic-server/usr/sbin
	mv --target-directory=debian/atomic-server/usr/sbin/ \
	 debian/atomic-server/usr/bin/atomic-server

execute_after_dh_auto_clean:
	[ ! -f Cargo.lock.orig ] || mv -f Cargo.lock.orig Cargo.lock

# build manpages
debian/atomic-cli.1: debian/%.1: debian/atomic-cli/usr/bin/%
	$(call _help2man,$@,$<)
	set -e; $(foreach x,new get set remove edit destroy search list,\
	 $(call _help2man,$@,$<,$x);)
debian/atomic-server.8: debian/%.8: debian/atomic-server/usr/bin/%
	$(call _help2man,$@,$<)
	set -e; $(foreach x,export import generate-dotenv show-config reset,\
	 $(call _help2man,$@,$<,$x);)

# custom target unused during official build
get-vendorlibs:
# preserve and use pristine Cargo.lock
	[ -f Cargo.lock.orig ] || cp Cargo.lock Cargo.lock.orig
	cp -f Cargo.lock.orig Cargo.lock

# modernize crates
	cargo update

# preserve needed crates
	cargo vendor --versioned-dirs debian/vendorlibs~
	rm -rf debian/vendorlibs
	mkdir debian/vendorlibs
	cd debian/vendorlibs~ && mv --target-directory=../vendorlibs $(VENDORLIBS)

# tolerate binary files in preserved code
	find debian/vendorlibs -type f ! -size 0 | perl -lne 'print if -B' \
	 > debian/source/include-binaries

# accept newer crates
	$(call _mangle,cookie,0.16,>= 0.16| <= 0.18,actix-web-4.9.0)
	$(call _mangle,cssparser,0.27,>= 0.27| <= 0.31,kuchikiki-0.8.2 selectors-0.22.0)
	$(call _mangle,cssparser,0.27.2,>= 0.27.2| <= 0.31,lol_html-1.2.1)
	$(call _mangle,env_logger,0.8.1,>= 0.8.1| <= 0.10,sled-0.34.7)
	$(call _mangle,h2,0.3.26,>= 0.3.26| <= 0.4,actix-http-3.9.0)
	$(call _mangle,hashbrown,0.13.1,>= 0.13.1| <= 0.14,lol_html-1.2.1)
	$(call _mangle,indexmap,1.6.0,>= 1.6| <= 2,kuchikiki-0.8.2)
	$(call _mangle,itertools,0.12.0,>= 0.12.0| <= 0.13,tantivy-0.22.0 tantivy-columnar-0.3.0)
	$(call _mangle,lru,0.11.0,>= 0.11| <= 0.12,tantivy-0.22.0)
	$(call _mangle,memmap2,0.7.1,>= 0.7.1| <= 0.9,tantivy-0.21.1)
	$(call _mangle,parking_lot,0.11.2,>= 0.11.2| <= 0.12,sled-0.34.7)
	$(call _mangle,path-slash,0.1,>= 0.1| <= 0.2,change-detection-1.2.0 static-files-0.2.4)
	$(call _mangle,phf,0.8,>= 0.8| <= 0.11,selectors-0.22.0)
	$(call _mangle,phf_codegen,0.8,>= 0.8| <= 0.11,selectors-0.22.0)
	$(call _mangle,servo_arc,0.1,>= 0.1| <= 0.3,selectors-0.22.0)
	$(call _mangle,zstd,0.12,>= 0.12| <= 0.13,tantivy-sstable-0.3.0)

# avoid WASM-only crates
	perl -gpi \
	 -e 's/\[target(\W+cfg\(target)?\W+wasm[^\n]*(\n[^\[\n][^\n]*)*//g;' \
	 debian/vendorlibs/ulid-1.1.3/Cargo.toml

# avoid windows-only crates
	perl -gpi \
	 -e 's/\[target\W+cfg\(windows[^\n]*(\n[^\[\n][^\n]*)*//g;' \
	 debian/vendorlibs/fs4-0.8.4/Cargo.toml \
	 debian/vendorlibs/socket2-0.5.5/Cargo.toml

# avoid abandoned crate instant
	perl -gpi \
	 -e 's/\[dependencies\.instant\](\n[^\[\n][^\n]*)*//g;' \
	 debian/vendorlibs/measure_time-0.8.3/Cargo.toml
	perl -gpi \
	 -e 's/\nuse/\nuse std::time::Instant;\nuse/;' \
	 -e 's/instant::Instant/Instant/g;' \
	 debian/vendorlibs/measure_time-0.8.3/src/lib.rs

# strip checksums for mangled crates
	echo '{"package":"Could not get crate checksum","files":{}}' \
	 | tee $(patsubst %,debian/vendorlibs/%/.cargo-checksum.json,\
	 actix-http-3.9.0 \
	 actix-web-4.9.0 \
	 change-detection-1.2.0 \
	 fs4-0.8.4 \
	 kuchikiki-0.8.2 \
	 lol_html-1.2.1 \
	 measure_time-0.8.3 \
	 selectors-0.22.0 \
	 sled-0.34.7 \
	 static-files-0.2.4 \
	 tantivy-0.22.0 \
	 tantivy-columnar-0.3.0 \
	 tantivy-sstable-0.3.0 \
	 ulid-1.1.3 \
	 )

	$(eval GONE = $(filter-out 0,\
	 $(shell grep -Fxc '      * package is missing' debian/TODO)))
	$(eval PENDING = $(filter-out 0,\
	 $(shell grep -Fxc '      * package is pending' debian/TODO)))
	$(eval INCOMPLETE = $(filter-out 0,\
	 $(shell grep -Fc '      * package lacks feature' debian/TODO)))
	$(eval BROKEN = $(filter-out 0,\
	 $(shell grep -c '      \* package is .*broken' debian/TODO)))
	$(eval UNWANTED = $(filter-out 0,\
	 $(shell grep -c '      \* package is unwanted' debian/TODO)))
	$(eval OLD = $(filter-out 0,\
	 $(shell grep -Fxc '      * package is outdated' debian/TODO)))
	$(eval AHEAD = $(filter-out 0,\
	 $(shell grep -Fxc '      * package is ahead' debian/TODO)))
	$(eval SNAPSHOT = $(filter-out 0,\
	 $(shell grep -Fxc '      * package should cover snapshot' debian/TODO)))
	$(eval REASONS = $(strip\
	 $(if $(GONE),$(GONE)_missing)\
	 $(if $(PENDING),$(PENDING)_pending)\
	 $(if $(BROKEN),$(BROKEN)_broken)\
	 $(if $(UNWANTED),$(UNWANTED)_unwanted)\
	 $(if $(INCOMPLETE),$(INCOMPLETE)_incomplete)\
	 $(if $(OLD),$(OLD)_outdated)\
	 $(if $(AHEAD),$(AHEAD)_ahead)\
	 $(if $(SNAPSHOT),$(SNAPSHOT)_unreleased)))
	$(eval c = ,)
	$(eval EMBEDDED = $(if $(REASONS),\n    ($(subst _,$() ,$(subst $() ,$c_,$(REASONS))))))
	@CRATES=$$(ls -1 debian/vendorlibs | grep -c .);\
	perl -gpi \
	 -e 's/FIXME: avoid most possible of embedded \K\d+ crates\n\h+[^\)\n]*\)/'"$$CRATES"' crates$(EMBEDDED)/;' \
	 debian/changelog;\
	echo;\
	echo "DONE: embedding $$CRATES crates$(EMBEDDED)";\
	echo
